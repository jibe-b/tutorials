# Apprendre des réussites des autres plutôt que partir du néant

## Embranchement pédagogique

Pour apprendre et comprendre des concepts et des méthodes qui ont été développées pendant des siècles,

- une stratégie beaucoup employée est de partir de zéro et de tout reconstruire
- git permet de partir d'un état et sans partir de zéro. Tout déconstruire pour tout comprendre est une des choses à faire. Ajouter des éléments pour voir si on est capable de comprendre est la seconde.

## 

Pour les mathématiques : partir d'une librairie existante

- la comprendre en lecture de code, et modification, refinement
- la compléter avec des fonctions supplémentaires, et pusher vers le répo pour tester si on a compris.


## Implémentations et retours d'expérience

-> Python-express
-> HackYourPhD Strasbourg
