# Tutorials

A set of tutorials I create or contribute to.

## Pedagogy strategy

[Learn from what giants have done](reverse-engineering-learning-strategy.md)

## Available tutorials

## Tutorials under construction

[Python-express](https://gitlab.com/open-scientist/python-express) (outside repo)

[OA Mag3](oa-mag3/)

Presentation given on Aug. 29th, 2017

[Jupyter notebooks for bioinformaticians](jupyter-for-bioinformaticians)

## Miscelleanous

- jupyter notebook
- RDF knowledge base + SPARQL queries -> TOC=>document

